#### MyProject

This is a simple project to illustrate use of GitLab CI's shared runners to build Docker Images

1. Build an image on commit to `master` branch
1. Place that image in the project's container registry
1. Run the container to show simple container execution

Note:  I pushed a fresh master branch with none of the history of me bungling through the setup.  I did this solely to set up for a clean pull should anyone actually use this.